echo off

rmdir bin\temp
mkdir bin\temp

bin\curl\curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{\"options\": {\"packageName\": \"Evento\",\"packageVersion\": \"0.0.1\",\"sourceFolder\": \"src\"}, \"swaggerUrl\": \"http://apievento.azurewebsites.net/swagger/v1/swagger.json\"}" "http://generator.swagger.io/api/gen/clients/csharp" | bin\jq .link > bin\temp\link

set /p link=<bin\temp\link

bin\curl\curl %link% --output bin\temp\evento-lib.zip

bin\unzip -o "bin\temp\evento-lib.zip" -d "bin\temp"

start libs\csharp\bin\compile.cmd
